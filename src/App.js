import "./App.css";
import { useEffect, useState } from "react";
import Home from "./component/home.js";
import { Route, Routes } from "react-router-dom";
import Show from "./component/show";
import Edit from "./component/edit";

function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/home" element={<Home />} />
        <Route path="/blog/:id" element={<Show />} />
        <Route path="/blog/edit/:id" element={<Edit />} />
      </Routes>
    </>
  );
}

export default App;
