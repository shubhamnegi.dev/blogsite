import { useNavigate, useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import "../App.css";

const Edit = () => {
  const navigate = useNavigate();
  const params = useParams();
  const id = params.id;
  const [dataObject, setDataObject] = useState({});
  const [dataTitle, setDataTitle] = useState("");
  const [dataAuthorname, setDataAuthorname] = useState("");
  const [dataDescription, setDataDescription] = useState("");
  const [click, setClick] = useState(0);

  useEffect(() => {
    fetch(`http://localhost:8000/Blogs/${id}`, {
      method: "GET",
    })
      .then((res) => res.json())
      .then((data) => {
        setDataObject(data);
        setDataTitle(data.title);
        setDataAuthorname(data.authorName);
        setDataDescription(data.Description);
      });
  }, [click]);

  function handleTitle(e) {
    setDataTitle(e.target.value);
  }
  function handleAuthor(e) {
    setDataAuthorname(e.target.value);
  }
  function handleDesc(e) {
    setDataDescription(e.target.value);
  }

  async function handleUpdate() {
    await fetch(`http://localhost:8000/Blogs/${id}`, {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        authorName: dataAuthorname,
        Description: dataDescription,
        title: dataTitle,
      }),
    });
    setClick(click + 1);
  }
  return (
    <div className="container">
      <div>Welcome to the blog site</div>
      <button onClick={() => navigate("/home")}>Home</button>
      <div className="box">
        Title: <input type="text" value={dataTitle} onChange={handleTitle} />
        <br />
        Author:{" "}
        <input type="text" value={dataAuthorname} onChange={handleAuthor} />
        <br />
        Description:{" "}
        <input type="text" value={dataDescription} onChange={handleDesc} />
        <div>
          <button onClick={handleUpdate}>Update</button>
        </div>
      </div>
    </div>
  );
};

export default Edit;
