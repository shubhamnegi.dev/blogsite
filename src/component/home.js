import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

const Home = () => {
  const [blogData, setBlogData] = useState([]);
  const [dataDelete, setDataDelete] = useState(false);
  const navigate = useNavigate();
  useEffect(() => {
    fetch("http://localhost:8000/Blogs", {
      method: "GET",
    })
      .then((res) => res.json())
      .then((data) => {
        setBlogData(data);
      });
  }, [dataDelete]);

  function handleDelete(id) {
    return async () => {
      await fetch(`http://localhost:8000/Blogs/${id}`, {
        method: "DELETE",
      });
      setDataDelete(!dataDelete);
    };
  }

  return (
    <div className="container">
      <div>Welcome to the blog site</div>
      <button onClick={() => navigate("/home")}>Home</button>
      {blogData.map((ele, index) => {
        return (
          <div key={index} className="box">
            <h4>Id: {ele.id}</h4>
            <h4>Title: {ele.title}</h4>
            <button onClick={() => navigate(`/blog/edit/${ele.id}`)}>
              edit
            </button>
            <button onClick={() => navigate(`/blog/${ele.id}`)}>show</button>
            <button onClick={handleDelete(`${ele.id}`)}>delete</button>
          </div>
        );
      })}
    </div>
  );
};

export default Home;
