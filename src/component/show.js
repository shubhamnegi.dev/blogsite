import { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
const Show = () => {
  const navigate = useNavigate();
  const [dataObject, setDataObject] = useState({});
  const params = useParams();
  const id = params.id;

  useEffect(() => {
    fetch(`http://localhost:8000/Blogs/${id}`, {
      method: "GET",
    })
      .then((res) => res.json())
      .then((data) => {
        setDataObject(data);
      });
  }, []);

  return (
    <div className="container">
      <div>Welcome to the blog site</div>
      <button onClick={() => navigate("/home")}>Home</button>
      <div className="box">
        <h3>{dataObject.id}</h3>
        <h3>{dataObject.title}</h3>
        <h3>{dataObject.Description}</h3>
        <h3>{dataObject.authorName}</h3>
      </div>
    </div>
  );
};

export default Show;
